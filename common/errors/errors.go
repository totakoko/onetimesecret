package errors

type AppError struct {
	Message  string
	HTTPCode int
}

func (e *AppError) Error() string {
	return e.Message
}

func InvalidParameter(message string) error {
	return &AppError{
		Message:  message,
		HTTPCode: 400,
	}
}

func MissingResource(message string) error {
	return &AppError{
		Message:  message,
		HTTPCode: 404,
	}
}

func ServerError(message string) error {
	return &AppError{
		Message:  message,
		HTTPCode: 500,
	}
}
