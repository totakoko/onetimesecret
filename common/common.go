package common

import (
	"context"
	"time"
)

type Store interface {
	Init() error
	StoreSecret(ctx context.Context, secret string, expiration time.Duration) (string, error)
	GetSecret(ctx context.Context, id string) (string, error)
}

type Secret struct {
	Id         string    `json:"id" binding:"required"`
	Content    string    `json:"content" binding:"required"`
	Expiration time.Time `json:"expiration" binding:"required"`
}
