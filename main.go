package main

import (
	"fmt"
	"os"
	"os/signal"
	"runtime"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/totakoko/onetimesecret/app"
	"gitlab.com/totakoko/onetimesecret/conf"
)

func main() {
	startTime := time.Now()
	log.Warn().Msg("application starting")

	defer func() {
		if r := recover(); r != nil {
			err, ok := r.(error)
			if !ok {
				err = fmt.Errorf("%v", r)
			}
			stack := make([]byte, 4<<10) // 4 KB
			length := runtime.Stack(stack, false)
			log.Error().Err(err).Bytes("stack", stack[:length]).Msg("application panic recovered")
		}
	}()

	config, err := conf.New()
	if err != nil {
		log.Fatal().Err(err).Msg("config error")
	}

	logLevel, err := zerolog.ParseLevel(config.LogLevel)
	if err != nil {
		log.Fatal().Err(err).Msg("could not parse log level")
	}
	zerolog.SetGlobalLevel(logLevel)

	application := app.New(config)
	err = application.Start()
	if err != nil {
		log.Fatal().Err(err).Msg("could not start application")
	}
	log.Warn().Dur("duration", time.Since(startTime)).Msg("application started")

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Warn().Msg("shutting down application")
	go func() {
		<-quit
		log.Fatal().Msg("application shutdown (forced)")
	}()
	err = application.Shutdown()
	if err != nil {
		log.Error().Err(err).Msg("application shutdown with error")
	} else {
		log.Warn().Msg("application shutdown")
	}
}
