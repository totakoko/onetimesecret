package app

import (
	"gitlab.com/totakoko/onetimesecret/conf"
	"gitlab.com/totakoko/onetimesecret/httpserver"
	"gitlab.com/totakoko/onetimesecret/store"
)

type Application struct {
	config conf.ApplicationConfig

	store      *store.Store
	httpServer *httpserver.HTTPServer
}

func New(config conf.ApplicationConfig) *Application {
	return &Application{
		config: config,
	}
}

func (a *Application) Start() error {
	a.store = store.New(a.config.Store)
	if err := a.store.Init(); err != nil {
		return err
	}

	a.httpServer = httpserver.New(a.config.HttpServer, a.config.PublicURL, a.store)

	err := a.httpServer.Start()
	return err
}

func (a *Application) Shutdown() error {
	if a.httpServer != nil {
		err := a.httpServer.Shutdown()
		if err != nil {
			return err
		}
	}
	return nil
}
