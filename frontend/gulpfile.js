const path = require('path')
const execFile = require('child_process').execFile
const { src, dest, series, parallel, watch } = require('gulp')
const stylus = require('gulp-stylus')
const pug = require('gulp-pug')
const errorHandler = require('gulp-error-handle')
const minifyCssNames = require('gulp-minify-css-names')
const csso = require('gulp-csso')
const kill = require('tree-kill')

process.on('uncaughtException', (err) => {
  console.log('Exception:', err)
  process.exit(2)
})

// changes to these paths must be correlated with changes in httpserver/assets.go
const buildDir = '.build'
const buildPublicDir = path.join(buildDir, 'public')
const buildTemplatesDir = path.join(buildDir, 'templates')

const frontendDir = 'frontend'
const publicDir = path.join(frontendDir, 'public')
const templatesDir = path.join(frontendDir, 'templates')
const stylesDir = path.join(frontendDir, 'styles')

function startServer () {
  const serverProcess = execFile('go', ['run', 'main.go'])
  serverProcess.stdout.pipe(process.stdout)
  serverProcess.stderr.pipe(process.stderr)
  return serverProcess
}

function debounce (func, wait) {
  let lastThis
  let lastArgs
  let timerId
  function wrapper (...args) {
    lastThis = this
    lastArgs = args
    if (timerId) {
      clearTimeout(timerId)
    }
    timerId = setTimeout(invoke, wait)
  }
  function invoke () {
    timerId = null
    func.apply(lastThis, lastArgs)
  }
  return wrapper
}

function assets () {
  return src(path.join(publicDir, '**/*'))
    .pipe(dest(buildPublicDir))
}

function css () {
  return src(path.join(stylesDir, '*.styl'))
    .pipe(errorHandler())
    .pipe(stylus())
    .pipe(csso())
    .pipe(dest(buildPublicDir))
}

function html () {
  return src(path.join(templatesDir, '*.pug'))
    .pipe(errorHandler())
    .pipe(pug())
    .pipe(dest(buildTemplatesDir))
}

function devServer () {
  watch(path.join(stylesDir, '**/*.styl'), css)
  watch(path.join(templatesDir, '**/*.pug'), html)
  watch(path.join(publicDir, '**/*'), assets)

  // debounce is used to limit the server restart signal
  const restartServer = debounce(function (done) {
    console.log('Restarting go server...')
    kill(serverProcess.pid, 'SIGTERM')
    serverProcess = startServer()
    console.log('restarted')
    done()
  }, 50)

  let serverProcess = startServer()
  watch(
    [
      '**/*.go',
      path.join(buildDir, '**/*')
    ],
    restartServer
  )
}

exports.build = series(
  parallel(css, html, assets),
  function () {
    return src([
      path.join(buildPublicDir, '*.css'),
      path.join(buildTemplatesDir, '*.html')
    ], { base: buildDir })
      .pipe(minifyCssNames({
        postfix: '',
        prefix: 'ots-'
      }))
      .pipe(dest(buildDir))
  }
)

exports.default = series(
  parallel(css, html, assets),
  devServer
)
