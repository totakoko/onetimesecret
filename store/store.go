package store

import (
	"context"
	"strconv"
	"time"

	"github.com/go-redis/redis"
	"github.com/rs/zerolog/log"
	"gitlab.com/totakoko/onetimesecret/common/errors"
	"gitlab.com/totakoko/onetimesecret/helpers"
	"gitlab.com/totakoko/onetimesecret/store/conf"
)

type Store struct {
	config      conf.StoreConfig
	redisClient *redis.Client
}

func New(config conf.StoreConfig) *Store {
	return &Store{
		config: config,
		redisClient: redis.NewClient(&redis.Options{
			Addr:     config.Addr,
			Password: config.Password,
			DB:       config.Database,
		}),
	}
}

/*
Init : establishes a connection to the redis server.
*/
func (s *Store) Init() error {
	err := s.redisClient.Ping().Err()
	if err != nil {
		return err
	}
	if s.config.Flush {
		log.Warn().Msg("Flushing database")
		err = s.redisClient.FlushDB().Err()
		return err
	}
	return nil
}

func (s *Store) StoreSecret(ctx context.Context, secret string, expiration time.Duration) (string, error) {
	if len(secret) > s.config.MaxSecretSize {
		return "", errors.InvalidParameter("secret is too long (limit is set at " + strconv.Itoa(s.config.MaxSecretSize) + " bytes)")
	}
	if expiration > s.config.MaxSecretExpiration {
		return "", errors.InvalidParameter("expiration is too high (limit is set at " + s.config.MaxSecretExpiration.String() + ")")
	}
	key := helpers.GenerateRandomString(s.config.KeyLength)
	_, err := s.redisClient.WithContext(ctx).Set(secretPath(key), secret, expiration).Result()
	log.Info().Str("key", key).Dur("expiration", expiration).Msg("stored secret")
	return key, err
}

func (s *Store) GetSecret(ctx context.Context, key string) (string, error) {
	log.Info().Str("key", key).Msgf("read secret")
	secretFullKey := secretPath(key)

	pipeline := s.redisClient.WithContext(ctx).TxPipeline()
	get := pipeline.Get(secretFullKey)
	pipeline.Del(secretFullKey)

	_, err := pipeline.Exec()
	switch err {
	case redis.Nil:
		return "", errors.MissingResource("missing secret")
	case nil:
		// we don't convert to object because it will be serialized anyway
		return get.Val(), nil
	default:
		return "", err
	}
}

func secretPath(key string) string {
	return "ots:secrets:" + key
}
