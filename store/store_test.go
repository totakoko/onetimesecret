package store

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/totakoko/onetimesecret/conf"
	"gitlab.com/totakoko/onetimesecret/helpers/tests"
	storeconf "gitlab.com/totakoko/onetimesecret/store/conf"
)

var (
	existingSecretKey string
)

func newConfig(t *testing.T) storeconf.StoreConfig {
	config, err := conf.New()
	if err != nil {
		t.Error(err)
	}
	return config.Store
}

func Test_StoreInit_OK(t *testing.T) {
	assert := tests.SetupTest(t)
	config := newConfig(t)
	store := New(config)

	err := store.Init()
	assert.NoError(err)
}

func Test_StoreInit_InvalidAddr(t *testing.T) {
	assert := tests.SetupTest(t)
	config := newConfig(t)
	config.Addr = "127.0.0.1:9999"
	store := New(config)

	err := store.Init()
	// the error is either getsockopt or connect...
	assert.Contains(err.Error(), "dial tcp 127.0.0.1:9999: ")
	assert.Contains(err.Error(), ": connection refused")
}

func Test_StoreStoreSecret_OK(t *testing.T) {
	assert, store := SetupValidStore(t)

	key, err := store.StoreSecret(context.Background(), "top-secret", 10*time.Second)
	assert.NoError(err)
	assert.Equal("OHoF8iVd", key) // first call to rand is constant with the same seed
}

func Test_Store_Expiration(t *testing.T) {
	assert, store := SetupValidStore(t)

	expectedKey := "OHoF8iVd"
	key, err := store.StoreSecret(context.Background(), "top-secret", 50*time.Millisecond)
	assert.NoError(err)
	assert.Equal(expectedKey, key) // first call to rand is constant with the same seed
	time.Sleep(30 * time.Millisecond)

	secret, err := store.GetSecret(context.Background(), expectedKey)
	assert.NoError(err)
	assert.Equal("top-secret", secret)
	time.Sleep(30 * time.Millisecond)

	secret, err = store.GetSecret(context.Background(), expectedKey)
	assert.EqualError(err, "missing secret")
	assert.Empty(secret)
}

func Test_Store_ErrMaxSize(t *testing.T) {
	assert := tests.SetupTest(t)
	config := newConfig(t)
	config.Flush = true
	config.MaxSecretSize = 14

	store := New(config)
	err := store.Init()
	assert.NoError(err)

	key, err := store.StoreSecret(context.Background(), "short secret", 10*time.Second)
	assert.NoError(err)
	assert.Equal("ONRhfKsU", key)

	key, err = store.StoreSecret(context.Background(), "too long secret", 10*time.Second)
	assert.Contains(err.Error(), "secret is too long")
	assert.Empty(key)
}

func Test_Store_ErrMaxExpiration(t *testing.T) {
	assert := tests.SetupTest(t)
	config := newConfig(t)
	config.Flush = true
	config.MaxSecretExpiration = 60 * time.Second

	store := New(config)
	err := store.Init()
	assert.NoError(err)

	key, err := store.StoreSecret(context.Background(), "secret", 30*time.Second)
	assert.NoError(err)
	assert.Equal("ONRhfKsU", key)

	key, err = store.StoreSecret(context.Background(), "secret", 61*time.Second)
	assert.Contains(err.Error(), "expiration is too high")
	assert.Empty(key)
}

func Test_StoreGetSecret_OK(t *testing.T) {
	assert, store := SetupValidStore(t)

	secret, err := store.GetSecret(context.Background(), existingSecretKey)
	assert.NoError(err)
	assert.Equal("existing top-secret", secret)
}

func Test_StoreGetSecret_Missing(t *testing.T) {
	assert, store := SetupValidStore(t)

	secret, err := store.GetSecret(context.Background(), "non-existing key")
	assert.EqualError(err, "missing secret")
	assert.Empty(secret)
}

func SetupValidStore(t *testing.T) (*require.Assertions, *Store) {
	assert := tests.SetupTest(t)
	config := newConfig(t)
	config.Flush = true

	store := New(config)
	err := store.Init()
	assert.NoError(err)

	existingSecretKey, err = store.StoreSecret(context.Background(), "existing top-secret", 10*time.Second)
	assert.NoError(err)
	return assert, store
}
