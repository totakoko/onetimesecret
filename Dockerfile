FROM node:18.4.0-alpine3.16 AS builder-node

WORKDIR /src

COPY package.json package-lock.json ./
RUN npm ci

COPY frontend ./frontend/
RUN npm run build


FROM golang:1.18.3-alpine3.16 AS builder-go

WORKDIR /go/src/gitlab.com/totakoko/onetimesecret
COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN go build -ldflags "-s -w"


FROM hairyhenderson/upx as builder-upx

COPY --from=builder-go /go/src/gitlab.com/totakoko/onetimesecret/onetimesecret /onetimesecret-unoptimized
RUN upx --best --lzma /onetimesecret-unoptimized -o /onetimesecret


FROM alpine:3.16.0

COPY --from=builder-node /src/.build /opt/ots/.build
COPY --from=builder-upx /onetimesecret /opt/ots/onetimesecret

WORKDIR /opt/ots/
EXPOSE 5000
CMD ["/opt/ots/onetimesecret"]
