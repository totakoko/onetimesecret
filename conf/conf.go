package conf

import (
	"github.com/kelseyhightower/envconfig"
	"gitlab.com/totakoko/onetimesecret/httpserver"
	storeconf "gitlab.com/totakoko/onetimesecret/store/conf"
)

type ApplicationConfig struct {
	ListenPort int    `default:"5000"`
	LogLevel   string `default:"info"`
	PublicURL  string `default:"http://localhost:5000/"`
	HttpServer httpserver.HTTPServerConfig
	Store      storeconf.StoreConfig
}

func New() (ApplicationConfig, error) {
	var conf ApplicationConfig
	err := envconfig.Process("OTS", &conf)
	return conf, err
}
