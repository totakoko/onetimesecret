package httpserver

import (
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
	"gitlab.com/totakoko/onetimesecret/common/version"
)

func (s *HTTPServer) registerAPIRoutes(router *echo.Group) {
	router.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			// response will always be JSON
			c.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSONCharsetUTF8)
			return next(c)
		}
	})
	router.GET("", s.APIInfos)
	router.POST("/secrets", s.APICreateSecret)
	router.GET("/secrets/:id", s.APIGetSecret)
}

func (s *HTTPServer) APIInfos(c echo.Context) error {
	return c.JSON(http.StatusOK, echo.Map{
		"version": version.ApplicationVersion,
	})
}

func (s *HTTPServer) APICreateSecret(c echo.Context) error {
	expiration, err := strconv.ParseUint(c.FormValue("expiration"), 10, 64)
	if err != nil {
		return err
	}
	secretKey, err := s.Store.StoreSecret(c.Request().Context(), c.FormValue("secret"), time.Duration(expiration)*time.Second)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusCreated, secretKey)
}

func (s *HTTPServer) APIGetSecret(c echo.Context) error {
	secret, err := s.Store.GetSecret(c.Request().Context(), c.Param("id"))
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, secret)
}
