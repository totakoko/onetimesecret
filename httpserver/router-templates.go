package httpserver

import (
	"errors"
	"html/template"
	"io"
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
	apperrors "gitlab.com/totakoko/onetimesecret/common/errors"
)

func (s *HTTPServer) registerTemplatesRoutes(router *echo.Group) {
	router.GET("/", s.DisplayHomePage)
	router.GET("/_offline", s.DisplayOfflinePage)
	router.GET("/about", s.DisplayAboutPage)
	router.POST("/secrets", s.CreateSecret)
	router.GET("/secrets/:id", s.GetSecret)
	router.GET("/secrets/:id/view", s.GetSecretContent)
}

func (s *HTTPServer) DisplayHomePage(c echo.Context) error {
	return c.Render(http.StatusOK, string(TemplateCreateSecret), nil)
}

func (s *HTTPServer) DisplayOfflinePage(c echo.Context) error {
	return c.Render(http.StatusOK, string(TemplateOffline), nil)
}

func (s *HTTPServer) DisplayAboutPage(c echo.Context) error {
	return c.Render(http.StatusOK, string(TemplateAbout), nil)
}

func (s *HTTPServer) CreateSecret(c echo.Context) error {
	expiration, err := strconv.ParseUint(c.FormValue("expiration"), 10, 64)
	if err != nil {
		return err
	}
	secretKey, err := s.Store.StoreSecret(c.Request().Context(), c.FormValue("secret"), time.Duration(expiration)*time.Second)
	if err != nil {
		return err
	}
	return c.Render(http.StatusCreated, string(TemplateViewSecretLink), echo.Map{
		"secretURL":  s.PublicURL + "secrets/" + secretKey,
		"expiration": c.FormValue("expiration") + " seconds",
	})
}

func (s *HTTPServer) GetSecret(c echo.Context) error {
	return c.Render(http.StatusOK, string(TemplateGetSecret), echo.Map{
		"secretViewURL": "/secrets/" + c.Param("id") + "/view",
	})
}

func (s *HTTPServer) GetSecretContent(c echo.Context) error {
	secret, err := s.Store.GetSecret(c.Request().Context(), c.Param("id"))
	switch err.(type) {
	case nil:
		return c.Render(http.StatusOK, string(TemplateViewSecret), echo.Map{
			"secret": secret,
		})
	case *apperrors.AppError:
		return c.Render(http.StatusNotFound, string(TemplateViewSecret), echo.Map{
			"secret": "Unknown secret",
		})
	default:
		return errors.New("unknown error")
	}
}

type TemplatesRenderer struct {
	templates *template.Template
}

func (t *TemplatesRenderer) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, string(name)+".html", data)
}

type AppTemplate string

const (
	TemplateOffline        AppTemplate = "_offline"
	TemplateAbout          AppTemplate = "about"
	TemplateCreateSecret   AppTemplate = "create-secret"
	TemplateGetSecret      AppTemplate = "get-secret"
	TemplateError          AppTemplate = "error"
	TemplateViewSecret     AppTemplate = "view-secret"
	TemplateViewSecretLink AppTemplate = "view-secret-link"
)
