package httpserver

import (
	"html/template"
	"path"
)

// changes to these paths must be correlated with changes in gulpfile.js
const publicPath = ".build/public"
const templatesPath = ".build/templates"

func loadTemplates() (*TemplatesRenderer, error) {
	template, err := template.ParseGlob(path.Join(templatesPath, "*.html"))
	if err != nil {
		return nil, err
	}
	templateRenderer := &TemplatesRenderer{
		templates: template,
	}
	return templateRenderer, nil
}
