package httpserver

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func (s *HTTPServer) registerTestTemplatesRoutes(router *echo.Group) {
	router.GET("/secret", s.TestSecret)
	router.GET("/link", s.TestCreateSecret)
	router.GET("/get", s.TestGetSecret)
}

func (s *HTTPServer) TestSecret(c echo.Context) error {
	return c.Render(http.StatusOK, string(TemplateViewSecret), echo.Map{
		"secret": "mysecret",
	})
}

func (s *HTTPServer) TestCreateSecret(c echo.Context) error {
	return c.Render(http.StatusCreated, string(TemplateViewSecretLink), echo.Map{
		"secretURL":  "/secrets/link",
		"expiration": "10 seconds",
	})
}

func (s *HTTPServer) TestGetSecret(c echo.Context) error {
	return c.Render(http.StatusOK, string(TemplateGetSecret), echo.Map{
		"secretViewURL": "/secrets/123/view",
	})
}
