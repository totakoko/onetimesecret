package httpserver

import (
	"context"
	"fmt"
	"net/http"
	"runtime"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/rs/zerolog"
	"gitlab.com/totakoko/onetimesecret/common/errors"
)

// inspired from https://github.com/ziflex/lecho/blob/3cf18943c98abe517c9ac68740579093481588cc/middleware.go
func RequestLogger(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		req := c.Request()
		res := c.Response()
		start := time.Now()

		err := next(c)
		stop := time.Now()
		var message string
		var logEvent *zerolog.Event
		if err != nil {
			message = "request errored"
			logEvent = logger.Error().Err(err)
		} else {
			message = "request completed"
			logEvent = logger.Info()
		}
		logEvent.
			Str("method", req.Method).
			Str("url", req.RequestURI).
			Int("statusCode", res.Status).
			Dur("responseTime", stop.Sub(start)).
			Msg(message)
		return err
	}
}

// inspired from https://github.com/labstack/echo/blob/3b07058a1d8f440497dc7be1d2ad9a2767c84a57/middleware/recover.go
func RequestRecover(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		defer func() {
			if r := recover(); r != nil {
				err, ok := r.(error)
				if !ok {
					err = fmt.Errorf("%v", r)
				}
				stack := make([]byte, 4<<10) // 4 KB
				length := runtime.Stack(stack, false)
				logger.Error().Err(err).Bytes("stack", stack[:length]).Msg("request panic recover")
				c.Error(err)
			}
		}()
		return next(c)
	}
}

func RequestTimeout(timeout time.Duration) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			ctx, cancel := context.WithTimeout(c.Request().Context(), timeout)
			defer cancel()
			c.SetRequest(c.Request().WithContext(ctx))
			err := next(c)
			if err == nil && ctx.Err() != nil {
				err = ctx.Err()
			}
			return err
		}
	}
}

func ErrorHandler(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		serviceErr := next(c)
		if serviceErr == nil || c.Response().Committed {
			return nil
		}
		var code int
		var message string

		switch errWithStatus := serviceErr.(type) {
		case *echo.HTTPError:
			code = errWithStatus.Code
			if stringMessage, ok := errWithStatus.Message.(string); ok {
				message = stringMessage
			} else {
				message = http.StatusText(code)
			}

		case *errors.AppError:
			code = errWithStatus.HTTPCode
			message = errWithStatus.Message
		default:
			code = http.StatusInternalServerError
			message = serviceErr.Error()
		}

		// Send the response
		var err error
		if c.Request().Method == http.MethodHead { // Issue #608
			err = c.NoContent(code)
		} else {
			if c.Response().Header().Get(echo.HeaderContentType) == echo.MIMEApplicationJSONCharsetUTF8 {
				// json
				err = c.JSON(code, echo.Map{
					"message": message,
				})
			} else {
				// html
				err = c.Render(code, string(TemplateError), echo.Map{
					"message": message,
				})
			}
		}
		if err != nil {
			logger.Error().Err(err).Msg("error handling error")
		}
		return serviceErr
	}
}
