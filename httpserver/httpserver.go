package httpserver

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/rs/zerolog/log"
	"gitlab.com/totakoko/onetimesecret/common"
)

var logger = log.With().Str("component", "http").Logger()

type HTTPServerConfig struct {
	Host                 string        `default:"0.0.0.0"`
	Port                 uint64        `default:"5000"`
	Timeout              time.Duration `default:"5s"`
	ReadHeaderTimeout    time.Duration `default:"15s"` // the amount of time allowed to read request headers
	ReadTimeout          time.Duration `default:"30s"` // the maximum duration for reading the entire request, including the body
	WriteTimeout         time.Duration `default:"5m"`  // the maximum duration before timing out writes of the response
	IdleTimeout          time.Duration `default:"30s"` // the maximum amount of time to wait for the next request when keep-alives are enabled.
	RequestTimeout       time.Duration `default:"3s"`
	ShutdownTimeout      time.Duration `default:"15s"`
	GzipCompressionLevel int           `default:"5"`
	DebugTemplates       bool          `default:"false"`
}

type HTTPServer struct {
	config    HTTPServerConfig
	PublicURL string
	Store     common.Store

	echo *echo.Echo
}

func New(config HTTPServerConfig, publicURL string, store common.Store) *HTTPServer {
	return &HTTPServer{
		config:    config,
		PublicURL: publicURL,
		Store:     store,
	}
}

func (s *HTTPServer) Start() (returnErr error) {
	listenAddress := s.config.Host + ":" + strconv.FormatUint(s.config.Port, 10)
	echoServer := echo.New()
	s.echo = echoServer
	templateRenderer, err := loadTemplates()
	if err != nil {
		return fmt.Errorf("template load error: %w", err)
	}
	s.echo.Renderer = templateRenderer
	echoServer.HideBanner = true
	echoServer.HidePort = true
	echoServer.Server = &http.Server{
		Addr:              listenAddress,
		IdleTimeout:       s.config.IdleTimeout,
		ReadHeaderTimeout: s.config.ReadHeaderTimeout,
		ReadTimeout:       s.config.ReadTimeout,
		WriteTimeout:      s.config.WriteTimeout,
	}
	echoServer.Use(RequestRecover)
	echoServer.Use(RequestLogger)
	echoServer.Use(ErrorHandler)
	echoServer.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
	}))
	echoServer.Use(middleware.GzipWithConfig(middleware.GzipConfig{
		Level: s.config.GzipCompressionLevel,
	}))
	echoServer.Static("/", publicPath)
	s.registerTemplatesRoutes(echoServer.Group(""))
	s.registerAPIRoutes(echoServer.Group("/api"))
	if s.config.DebugTemplates {
		s.registerTestTemplatesRoutes(echoServer.Group("/test"))
	}

	// Start server
	go func() {
		err := echoServer.StartServer(echoServer.Server)
		if err != nil {
			returnErr = err
		}
	}()

	tryCount := 100
	for tryCount > 0 {
		_, err := http.Get("http://" + listenAddress + "/api")
		if err == nil {
			break
		}
		time.Sleep(10 * time.Millisecond)
		tryCount--
	}
	if tryCount == 0 {
		return errors.New("server did not start in time")
	}
	if returnErr == nil {
		logger.Warn().Str("addr", listenAddress).Msg("server listening")
	}
	return returnErr
}

func (s *HTTPServer) Shutdown() error {
	ctx, cancel := context.WithTimeout(context.Background(), s.config.ShutdownTimeout)
	defer cancel()
	return s.echo.Shutdown(ctx)
}
